Name:          leveldb-java
Version:       0.12
Release:       1
Summary:       LevelDB Java Port
License:       ASL 2.0
URL:           https://github.com/dain/leveldb
Source0:       https://github.com/dain/leveldb/archive/%{version}/leveldb-%{version}.tar.gz

BuildRequires: mvn(com.google.guava:guava) mvn(org.xerial.snappy:snappy-java) mvn(joda-time:joda-time)
BuildRequires: mvn(junit:junit) mvn(org.testng:testng) mvn(org.fusesource.leveldbjni:leveldbjni-all)
BuildRequires: maven-local maven-enforcer-plugin maven-site-plugin maven-surefire-provider-testng
BuildRequires: mvn(org.apache.maven.plugins:maven-checkstyle-plugin)

BuildArch:     noarch

%description
Java rewritten LevelDB port.

%package help
Provides:      %{name}-javadoc = %{version}-%{release}
Obsoletes:     %{name}-javadoc < %{version}-%{release}
Summary:       Leveldb-java java usage introduction

%description help
This package contains introduction to using leveldb-java.

%prep
%autosetup  -n leveldb-%{version} -p1

%pom_remove_parent
%pom_remove_plugin :maven-shade-plugin leveldb
%pom_add_dep junit:junit::test leveldb
sed -i 's/${air.main.basedir}/./g' pom.xml
%pom_xpath_inject "pom:properties" "<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>"
%pom_xpath_inject "pom:properties" "<encoding>UTF-8</encoding>"
%pom_xpath_inject "pom:properties" "<java.version>1.8</java.version>"
%pom_xpath_inject "pom:properties" "<maven.compiler.source>1.8</maven.compiler.source>"
%pom_xpath_inject "pom:properties" "<maven.compiler.target>1.8</maven.compiler.target>"
%pom_xpath_inject "pom:dependency[pom:artifactId='testng']" '<version>6.14.3</version>' leveldb

%build
%mvn_build

%install
%mvn_install

%files  -f .mfiles
%dir %{_javadir}/%{name}
%doc README.md
%license license.txt notice.md

%files help -f .mfiles-javadoc

%changelog
* Thu Jan 18 2024 Ge Wang <wang__ge@126.com> - 0.12-1
- Update to version 0.12

* Thu Jan 16 2020 lihao <lihao129@huawei.com> - 0.7-11
- Delete patches

* Fri Dec 20 2019 yangjian<yangjian79@huawei.com> - 0.7-10
- Package init
